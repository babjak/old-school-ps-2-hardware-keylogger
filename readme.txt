﻿A hardware keylogger for PS\2 type keyboards. The
keyboard-microcontroller interface shall record any
subsequent keys pressed after a given key combination
is detected. The recorded character sequence can later
be recalled with e.g., a different key combination.