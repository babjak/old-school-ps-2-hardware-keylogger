set file_name=PS2_HW_keylogger
del %file_name%.html
del %file_name%.pdf

python C:\Python27\Scripts\rst2html.py .\%file_name%.rst --math-output=MathJax ./%file_name%.html
python C:\Python27\Scripts\rst2latex.py .\%file_name%.rst --stylesheet=BenRSTextra.tex ./%file_name%.tex
texify --pdf --tex-option=-synctex=1 --clean %file_name%.tex
del %file_name%.tex
del %file_name%.log
