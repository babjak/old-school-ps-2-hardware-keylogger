==================================
Old school PS/2 hardware keylogger
==================================
---------------------
Benjamin Babjak, 2014
---------------------


Specification
=============

The problem statement
---------------------

PC keyboard-microcontroller interface. The controller shall record any subsequent keys pressed after a given key combination is detected. [...] The recorded character sequence can later be recalled with e.g., a different key combination. [...]

The user manual
---------------

The device has two connecting ports: One that receives the PS/2 type keyboard (input), while the other port is used by the microcontroller to connect to the PC keyboard input (output). The power is supplied by the computer.

The so called scancodes of the keys pressed are stored by the microcontroller in an external EEPROM unit, which communicates via SPI. Stored data are not erased on turn off, after reboot they are still available. 

During its operation the microcontroller can assume different states. It examines the bytes it received at its input for state changing key sequences (see codes below). If any of the previously programmed codes are recognised, the according state is assumed.

The states
..........
*	In "base" or "transparent" state the device is transparent in the sense that bytes arriving at its input are forwarded to its output after the aforementioned analysis.
*	In the "recording" state every byte at the input is stored and forwarded towards the PC. With regards to the actual recording process two further states can be distinguished within the "recording" state. In case of the "recording-1" state the recording is endless, meaning that if the available memory becomes full, the new arriving bytes keep overwriting the oldest. In case of the "recording-2" state upon memory fill up the microcontroller switches to "transparent" state.
*	In the "playback" state the microcontroller sends previously recorded bytes to the output. Any type of activity originating from the PC or the keyboard terminates this state, and the microcontroller switches to "transparent" state.

Considering the distinctive features of the Hungarian language and the Hungarian keyboard layout the following codes are employed by the device:

+--------------+----------------+--------------+
| Key sequence | starting state | ending state |
+==============+================+==============+
| qweasdyxc    | transparent    | recording-1  |
+--------------+----------------+--------------+
| wersdfxcv    | transparent    | recording-2  |
+--------------+----------------+--------------+
|              | transparent    | playback     |
|              +----------------+--------------+
| ertdfgcvb    | recording-1    | transparent  |
|              +----------------+--------------+
|              | recording-2    | transparent  |
+--------------+----------------+--------------+

Furthermore there is an additional code that erases all the stored bytes: "bvcgfdtre"


Functional block diagram
========================

The system tasks can be divided the following way:

.. figure:: figures/Block_diagram.png
   :align: center
   :width: 60%

The function of the parts are as follows:

Keyboard interface
------------------
Process keyboard data, forward to PC.

PC interface
------------
Process PC data, forward to keyboard.

Counter
-------
Counting bits coming form the keyboard. Only the 8 data bits are used form the 11 that are sent at each key press.

States
------
The system's state employing the FLAGS register.

Last byte
---------
The last relevant byte from the keyboard. 
(Every key press generates 3 bytes: 
1.	Key scancode
2.	Key release indicating byte
3.	Key scancode again
Although all three codes are recorded, the state changes only utilise the last one!)

9 byte storage
--------------
The last 9 released keys received from keyboard. The stored bytes are compared to the state changing sequences.

State changing sequences
------------------------
Key sequnces capable of changing the system state.

Logic
-----
Implements complex behaviour processing the values of the other parts: records, changes state, looks for state changing codes. (In practice this is the main program itself combining individual functions.)

Read, write pointer
-------------------
These pointers define which addresses have to be accessed to read and write. These addresses are stored in the internal EEPROM of the microcontroller.

EEPROM
------
Long term storage of received bytes. (The EEPROM IC itself.)

Playback
--------
Plays back recorded bytes towards the PC.

Timer
-----
At playback timing might be needed, because of the sheer amount of stored bytes, which may not be directly replayable.


Hardware-software separation
============================

For the problem the above defined blocks are implemented in software, except for the EEPROM (which is an external hardware unit) and the PC, keyboard interface (which require PS/2 connectors).


Hardware system design
======================

The task solution requires only a few hardware components, their logical connection can be defined the following way:

.. figure:: figures/HW_block_diagram.png
   :align: center
   :width: 40%

The microcontroller (PIC 16F84A or AVR ATmega8) connects via PS/2 connectors to the PC and the keyboard. Through four wires (SPI) it connects to an external 4 kbit EEPROM (25LC040). On top of that a crystal oscillator and some capacitors might be needed. (AVR implementation has no such elements, PIC has a 4 MHz crystal oscillator and two 22 pF capacitors.)


Software system design
======================

The software structure can be divided to a main program and routines. It contains no interrupts, the global interrupt is blocked. The main program's task is to monitor changes on its pins (PIC: PORTB, AVR: PORTD), evaluate said changes, and - if the system is in the replay state - to call routines that output the memory (EEPROM) content. Routines provide a single dedicated service.

Employed registers and their task
---------------------------------

FLAGS
.....
The program can assume different states during its operation, which is stored in the FLAGS register. FLAGS register is 8 bit wide, bits are numbered from 0 to 7. The purpose of the FLAGS register bits is shown in the following table. (X marks unused bits.) FLAGS is first written during initialization.

+-------------+-----------------+---+---+---+--------------+-----------------+-----------------+------------+
| Bit number  |               7 | 6 | 5 | 4 |            3 |               2 |               1 |          0 |
+-------------+-----------------+---+---+---+--------------+-----------------+-----------------+------------+
| Description | Another byte    |   |   |   | Release byte |                 | Stops recording |            |
|             | to be processed | X | X | X | received     | playback state  | when full       | base state |
+-------------+-----------------+---+---+---+--------------+-----------------+-----------------+------------+
| Reset       |                 |   |   |   |              | PC_KB_TRANSFER, |                 |            |
|             |                 |   |   |   |              | KB_PC_TRANSFER, |                 |            |
|             | main            | X | X | X | main         | main            | Set_State       | Set_State  |
+-------------+-----------------+---+---+---+--------------+-----------------+-----------------+------------+
| Set         |                 |   |   |   |              |                 |                 | Set_State, |
|             | KB_PC_TRANSFER  | X | X | X | main         | Set_State       | Set_State       | main       |
+-------------+-----------------+---+---+---+--------------+-----------------+-----------------+------------+

TRDATA
......
Stores the last byte received from the keyboard.

RD_ROM, (OLV_ROM)
.................
The read pointer. Read address of the internal EEPROM. (The AVR compiler threw an error when using the "RD_ROM" name, thus it was renamed "OLV_ROM")

WR_ROM
......
The write pointer. Write address of the internal EEPROM.

COUNTER, COUNT
..............
They serve general counting purposes.

TEMP, TMP, TMP2
...............
These serve temporary storing purposes.

EEPROM_RD_L, EEPROM_RD_H
........................
These two registers specify the upper and lower address bytes when reading the external EEPROM.
		 
EEPROM_WR_L, EEPROM_WR_H
........................
These two registers specify the upper and lower address bytes when writing the external EEPROM.

RECEIVE, (VETT)
...............
Stores the last byte sent from the PC. (The AVR compiler threw an error when using the "RECEIVE" name, thus it was renamed "VETT")

PARITY
......
Register employed for parity calculation.

Q0, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8
..................................
These registers store the last received 9 bytes. They play a role when looking for state changing codes. (Q0 is the most recent, Q8 is the oldest.)

W
.
The name of the accumulator register for the PIC. In order for the PIC algorithm to be portable to AVR, this register had to be established for the AVR as well.

Software components
-------------------

init
....

Called once before the main program after reset. Its task is to reset used registers, (as the program defines various states during its operation) set up the inital state, and to set up the output.

main
....
The main program, which contains the forever loop, can be divided into three parts. 

In the first part the value of the clock signal (PC_CLOCK, KB_CLOCK) is examined. In case these are not high level, communication has started, which is handled by the main program calling the right routine. If the keyboard pulled down the clock, the KB_PC_TRANSFER routine, if the PC pulled down the clock, the PC_KB_TRANSFER routine is called.

The second part examines whether a valid byte was received from the keyboard or not. If yes, it pulls down the keyboard's clock to have enough time for porcessing. During processing the current byte is stored (given the current state requires recording). After that the state may be changed based on the last received 9 bytes using the Queue and Set_State routines. To change states one of the defined 3+1 state changing codes have to be received by the program.

The third part is responsible for the playback of recorded bytes, if the system is in the playback state. It reads the EEPROM and sends the bytes to the PC using the ByteOut routine.

Routines
--------

(PIC:) Delay_Routine, Delay macro, (AVR:) delayus
.................................................
On the PIC microcontroller this routine is responsible for accurate delay generation. The Delay_Routine counts down, the time can be specified with the value given in W. This way the system can wait for 4W+4 cycles. The Delay macro makes it possible to wait for an arbitrary (not just a multiple of four) number of cycles by inserting nop and goto statements. 

In case of the AVR the program will wait for as many operations as was specified in the parameter. In practice one operation takes 1 us.

SPI_rd
......
Reads the external EEPROM using SPI. First it writes the reading command, then the address (EEPROM_RD_L, EEPROM_RD_H), finally it reads a byte and stores it in W.

SPI_wr
......
Writes the external EEPROM using SPI. First it writes the writing command, then it reads the response. If writing is approved, the writing command is issued, then it sends the writing address (EEPROM_WR_L, EEPROM_WR_H), finally the byte stored in W.

KB_PC_TRANSFER
..............
This routine is responsible for the data transfer from the keyboard to the PC. First it turns off the playback state. After this any change happening on the keyboard side is immediately forwarded to the PC. If the PC pulls the clock low during its operation, the routine stops. Every successful byte transfer will have byte stored in the TRDATA register, and signals the main program that a new byte to be processed was received (FLAGS 7. bit).

KB_PC_TRANSFER
..............
This routine is responsible for the data transfer from the PC to the keyboard. First it turns off the playback state. After this any change happening on the PC side is immediately forwarded to the keyboard. The routine ends when the keyboard sends the ACK bit at the end of the transmission.

ByteOut
.......
This routine sends the byte in W to the PC. It calculates the parity bit, and takes care of accurate timing.

Queue
.....
It copies the byte in TRDATA to the beginning of a queue storing the last 9 values. The oldest value is discarded.

Set_State
.........
It looks for state changing codes in the queue storing 9 bytes. Based on the codes it changes the system state:

*	qweasdyxc	base -> recording-1
*	wersdfxcv	base -> recording-2
*	ertdfgcvb	base -> playback state; recording -> base

There is one more code, which doesn't change states. It is "bvcgfdtre", which erases recorded data. In fact recorded values are not lost, merely the read and write pointers, stored in the microcontroller internal EEPROM, are reinitialised.

If the routine does not find a code, there are no changes.

etc.
....
Beside the above the program also includes simple routines for address manipulation, which help handling the external EEPROM read and write addresses.


Experience
==========

PIC vs AVR
----------
I've solved the above specified problem with two distinct microcontroller type, I've utilised a PIC 16F84A and an AVR ARmega8. The ATmega8 is a formidable microcontroller, which is capable of handling much more complex tasks as well, so it is an overkill for this particular task. The first choice was not the ATmega8 but the ATtiny2313. However, I could not successfully program this microcontroller type, thus I chose the more reliable ATmega8 solution.

There are several differences between the two microcontroller types. Both controllers follow the RISC concept, but there is a significant difference in the number of supported operations. In case of PIC there are 35, in case of AVR there are 90 or even more (~130) available operations. 

From a practical point of view, one of the biggest differences is that the PIC provides memory organised in banks. Even though most problems may be solved with a single bank, at least when handling the peripheries bank switches are unavoidable. For example, the direction and data registers of a given port are available in different banks.

In general regarding the speed it can be said that the AVR executes 1 operation using 1 cycle, while the PIC requires 4 cycles. This way the processing power of the AVR is 1-16 MIPS, while the PIC is 1-10 MIPS. Because the main task of these processing units is primarily controlling and not computing this difference does not affect their usability.

In case of the PIC most of the operations involve the W accumulator register. For example, only via the W register can we write a constant to an address. In case of the AVR 32 registers are available, with which arbitrary operations may be executed. Except for the exceptions of course, for example, only the upper 16 registers can be written with constants. 

In case of the PIC the handling of peripherals can be said to be simpler. For the PIC different ports are accessed with the same commands used for the memory as well, because peripherals can be interpreted as part of the memory. For the AVR separate commands exist for writing and reading peripherals.

In case of the PIC every port has two registers: A direction register (every pin can be set separately to be an input or output), and a data register (containing the bit to be written to a pin, or read from the pin). In case of the AVR each port has three registers, because the data register is divided into two. One register only holds read values, while the other only holds values to be written. This approach eliminates one of the PIC's problem as well. Assume that the first pin of one of the ports of the PIC is an input, while the second one is an output. If the value of the second pin (output) changes, the data register bit affiliated with the first pin also changes. Although there was no explicit read operation executed for the first pin, the writing of the second pin automatically involves a read. During this any value on the first pin will end up as the first bit of the data register.

This problem proved to be crucial, as during the handling of the keyboard (as opposed to SPI) the pin's direction was not defined a priori. The direction continually changed throughout the operation, thus it could not be guaranteed that an output-input-output transition of a pin will output the same value at both output states. Again emphasizing that this change happened despite not executing a read operation for the given pin.

Further smaller changes were observable regarding the used commands. Both controllers provide a way to examine a given bit of a register, however the setting of bits is different. In case of the PIC when a single bit of a register is changed the number of the bit has to be specified. In case of the AVR the bit setting does not use a number but a mask. This way several bits may be set at the same time.

Differences can be experienced with the energy consumption of the two employed microcontrollers as well. The ATmega8 microcontroller malfunctioned in environments where the PIC 16F84A worked perfectly. With the replacement of the keyboard, however, it fulfilled its task flawlessly. The error is probably due to the increased power intake of the ATmega8.


EEPROM
======

I've came across two mistakes or shortcomings of the documentation during programming the EEPROM. 

The EEPROM documentation defines an algorithm, with which, if followed step-by-step, the EEPROM programming becomes possible. The first step of this is to query the EEPROM's state, which tells whether there is a write in process at the given time instance. Only after the EEPROM has finished the storage of the previous byte can the new value be processed. Experience shows that the EEPROM state reply will always allow writing, even if a previous write is still being processed. Because of this the current write is always interrupted at a new write request. To solve the problem the microcontroller will wait 5 ms after each write.

Another lack in the documentation concerns the protected memory areas. The EEPROM supports to set certain memory regions protected. This way if we initiate a write to a protected area in the end the byte will not get written. Defining protected areas, however, is not optional but a mandatory step, which has to take place at every initialization.


Schematics
==========

.. figure:: figures/avr.png
   :align: center
   :width: 75%

.. figure:: figures/pic.png
   :align: center
   :width: 75%

.. figure:: figures/csatl.png
   :align: center
   :width: 75%
